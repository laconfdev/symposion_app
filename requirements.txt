Django>=2.2
pinax-theme-bootstrap==8.0.1
pinax-eventlog[django-lts]==5.1.0
django-formset-js==0.5.0
whitenoise==5.2.0
dj-database-url==0.5.0
pylibmc==1.6.1
django-debug-toolbar==3.1.1
django-bootstrap-form==3.4
django-settings-export~=1.2.1
django-capture-tag==1.0
djangosaml2==0.50.0
django-gapc-storage==0.5.2
django-waffle==2.0.0

# database
mysqlclient==2.0.1

# For testing
django-nose==1.4.7
coverage==5.3
factory_boy==3.1.0

# Symposion reqs
django-appconf==1.0.4
django-model-utils==4.0.0
django-reversion==3.0.8
django-sitetree==1.16.0
django-taggit==1.3.0
django-timezone-field==4.1.2
easy-thumbnails==2.8.5
bleach==3.2.1
pytz>=2020.1
django-ical==1.7.1

# Registrasion reqs
django-nested-admin==3.3.2
CairoSVG==2.4.2
PyJWT==2.0.0

# Registripe
django-countries==7.3.2
pinax-stripe==4.4.0
requests==2.24.0
stripe==2.55.0

# Regidesk
python-barcode==0.15.1

# SASS Compiler and template tags
libsass==0.20.1
django-sass-processor==0.8.2
django-compressor==2.4

django-crispy-forms==1.9.2
