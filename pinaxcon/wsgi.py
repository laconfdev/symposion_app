import os

os.environ.setdefault("DEBUG", "0")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pinaxcon.settings")

from django.core.wsgi import get_wsgi_application  # noqa

application = get_wsgi_application()
