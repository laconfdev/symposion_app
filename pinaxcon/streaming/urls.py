from django.conf.urls import url
from pinaxcon.streaming import views


urlpatterns = [
    url(r"^$", views.streaming_view, name="streaming_home"),
    url(r'^feeds$', views.streaming_feeds, name="streaming_feeds"),
]
