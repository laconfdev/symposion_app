from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class StreamingConfig(AppConfig):
    name = "pinaxcon.streaming"
    label = "pinaxcon_streaming"
    verbose_name = _("Pinaxcon Streaming")
    admin_group_name = "Streaming Admins"

    def get_admin_group(self):
        from django.contrib.auth.models import Group

        group, created = Group.objects.get_or_create(name=self.admin_group_name)
        return group
