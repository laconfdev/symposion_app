from django.contrib import admin

from pinaxcon.streaming import models


class RoomStreamAdmin(admin.ModelAdmin):
    list_display = ('room', 'day', 'stream_id', 'chat_url', 'published',)
    list_filter = ('room','day','published',)


admin.site.register(models.RoomStream, RoomStreamAdmin)
