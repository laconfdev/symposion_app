import base64
import datetime
import json
import jwt

from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings

from django.contrib.auth.decorators import login_required

from registrasion.controllers.item import ItemController
from regidesk.models import CheckIn
from pinaxcon.streaming.models import RoomStream


@login_required
def streaming_view(request):
    ctx = {}
    return render(request, 'streaming/overview.html', ctx)


@login_required
def streaming_feeds(request):
    """Details of the currently available live streams."""
    stream_data = []
    user_data = {}

    # Find the checkin for the current user.
    checkin = _current_checkin(request)

    if checkin:
        user_data["code"] = checkin.code

        # Find all streams for the current day
        today = datetime.datetime.now().date()
        streams = RoomStream.objects.filter(
            day__date = today,
            published = True,
        )

        for stream in streams:
            stream_details = {
                "stream_id": stream.id,
                "room_id": stream.room.id,
                "room_name": stream.room.name,
                #"track_name": stream.room.track.name,
                "stream_name": str(stream),
                "playback_id": stream.playback_id,
                "video_token": _mux_token(checkin, stream.playback_id, 'v'),
                "thumbnail_token": _mux_token(checkin, stream.playback_id, 't'),
                "chat_url": stream.chat_url,
            }
            stream_data.append(stream_details)

    live_streams_data = {
        "ui_version": settings.STREAMING_UI_VERSION,
        "streams": stream_data,
        "user": user_data,
    }

    return HttpResponse(
        json.dumps(live_streams_data, indent=2),
        content_type="application/json"
    )


def _current_checkin(request):
    # Check that they have a ticket first (only item from category 1)
    TICKET_CATEGORY = 1
    items = ItemController(request.user).items_purchased(
        category=TICKET_CATEGORY
    )

    if not items:
        return None

    # Get token from checkin
    checkin = CheckIn.objects.get_or_create(user=request.user)[0]
    return checkin


def _mux_token(checkin, playback_id, audience):
    """Returns the Mux JSON Web Token (JWT) for a given video (playback_id)
    that is linked to this checkin."""
    if not checkin.code or not settings.MUX_PRIVATE_KEY_BASE64 or not settings.MUX_SIGNING_KEY_ID:
        return None

    # Based on example at https://docs.mux.com/guides/secure-video-playback
    private_key = base64.b64decode(settings.MUX_PRIVATE_KEY_BASE64)
    expiry_date = settings.CONF_END + datetime.timedelta(days=1)

    token = {
        'sub': playback_id,
        'exp': expiry_date.timestamp(),
        'aud': audience,
        'custom': {
            'checkin': checkin.code,
        }
    }
    headers = {
        'kid': settings.MUX_SIGNING_KEY_ID
    }
    json_web_token = jwt.encode(token, private_key, algorithm="RS256", headers=headers)
    return json_web_token
