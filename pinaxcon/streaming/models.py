import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from symposion.schedule.models import Day, Room


class RoomStream(models.Model):
    """Video/Chat stream information for a room at the conference."""

    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    stream_id = models.CharField(max_length=255, verbose_name=_("Stream Identifier"), null=True)
    playback_id = models.CharField(max_length=255, verbose_name=_("Playback Identifier"), null=True)
    chat_url= models.CharField(max_length=255, verbose_name=_("Chat URL"), null=True)
    published = models.BooleanField(default=True, verbose_name=_("Published"))

    def __str__(self):
        return "%s - %s" % (self.room.name, self.day.date.strftime("%a"))

    class Meta:
        unique_together = [('room', 'day')]
        verbose_name = _("Room Stream")
        verbose_name_plural = _("Room Streams")
