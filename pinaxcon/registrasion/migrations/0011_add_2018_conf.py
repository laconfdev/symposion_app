from django.db import migrations


def populate(apps, schema_editor):
    PastEvent = apps.get_model("pinaxcon_registrasion","PastEvent")
    PastEvent.objects.get_or_create(
        year=2018, defaults={'name': '2018 Sydney'}
    )


class Migration(migrations.Migration):

    dependencies = [
        ('pinaxcon_registrasion', '0010_auto_20180929_1530'),
    ]

    operations = [
        migrations.RunPython(populate)
    ]
