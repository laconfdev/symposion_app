from django.contrib import admin

from symposion.reviews.models import NotificationTemplate, ProposalResult, ResultNotification, Review


admin.site.register(
    NotificationTemplate,
    list_display=[
        'label',
        'from_address',
        'subject'
    ]
)

admin.site.register(
    ProposalResult,
    list_display=['proposal', 'status', 'score', 'vote_count', 'accepted']
)

admin.site.register(
    ResultNotification,
    list_display=['proposal','timestamp','to_address','subject'],
    search_fields = ['proposal__title','to_address','subject'],
    readonly_fields = ['proposal','timestamp','template','to_address','from_address','subject','body']
)

admin.site.register(
    Review,
    list_display=[
        'proposal',
        'user',
        'vote',
        'submitted_at',
    ]
)
