from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from symposion.proposals.models import ProposalSection


@login_required
def dashboard(request):
    if request.session.get("pending-token"):
        return redirect("speaker_create_token", request.session["pending-token"])

    # Patching available proposal kinds into the dashboard to enable creating
    # proposals from the dashboard directly.    
    sections = ProposalSection.available().prefetch_related('section__proposal_kinds')
    kinds = [kind for section in sections for kind in section.section.proposal_kinds.all()]

    return render(request, "dashboard.html", {'proposal_kinds': kinds})
