from django.conf.urls import url

from regidesk import views

app_name='regidesk'
urlpatterns = [
    url(r"^boardingpass", views.boardingpass, name="boardingpass"),
    url(r"^([A-Z0-9]{6}$)", views.boarding_overview, name="checkin_detail"),
    url(r"^([A-Z0-9]{6}).png$", views.checkin_png, name="checkin_png"),
    url(r"^overview/([a-z]+)?$", views.boarding_overview, name="boarding_overview"),
    url(r"^prepare_passes/", views.boarding_prepare, name="boarding_prepare"),
    url(r"^send_passes/", views.boarding_send, name="boarding_send"),
    url(r"^checkin/$", views.CheckInLanding.as_view(), name="check_in_scanner"),
    url(r"^checkin/(?P<access_code>[A-Z0-9]{6})/$", views.check_in_overview, name="check_in_user_view"),
    url(r"^checkin/([A-Z0-9]{6})/badge$", views.checken_in_badge, name="check_in_badge"),
    url(r"^checkin/([A-Z0-9]{6})/barcode.png$", views.checkin_barcode_png, name="checkin_barcode_png"),
    url(r"^$", views.redir_main, name="checkin_choose"),
]
