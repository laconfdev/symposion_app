$(function() {
    const ROOM_REFRESH_INTERVAL_MS = 10 * 60 * 1000;
    const holdingFrame = $('#holding-frame');
    const streamingFrame = $('#streaming-frame');
    const roomsFrame = $('#rooms-frame');
    let lastVersion = '';
    let selectedStream = null;
    let streamData = null;

    function change_room(stream, user_details) {
        if (selectedStream && selectedStream.id === stream.stream_id && selectedStream.playbackId === stream.playback_id) {
            // No change, leave as is.
            return;
        }

        selectedStream = {
            id: stream.stream_id,
            playbackId: stream.playback_id,
        };

        streamingFrame.empty();

        let roomInfo = $('<div class="room-info"><h2>Currently Viewing: ' + stream.room_name + '</h2></div>');
        streamingFrame.append(roomInfo);

        const muxPlayer = document.createElement("mux-player");
        muxPlayer.setAttribute("playback-id", stream.playback_id);
        muxPlayer.setAttribute("metadata-video-title", stream.stream_name);
        muxPlayer.setAttribute("metadata-viewer-user-id", user_details.code);
        muxPlayer.setAttribute("autoplay", true);
        muxPlayer.tokens = {
            playback: stream.video_token,
            thumbnail: stream.thumbnail_token,
        };
        streamingFrame.append(muxPlayer);
    }

    function update_streams(data) {

        if (lastVersion && data.ui_version != lastVersion) {
            window.location.reload();
            return;
        }

        lastVersion = data.ui_version;

        // If no streams, display holding message.
        if (data.streams.length == 0) {
            holdingFrame.show();
            streamingFrame.hide();
            roomsFrame.hide();
            return;
        }

        holdingFrame.hide();
        streamingFrame.show();
        roomsFrame.show();

        streamData = data;

        var stream = null;
        if (selectedStream && selectedStream.id) {
            for (var i = 0; i < data.streams.length; ++i) {
                var availableStream = data.streams[i];
                if (availableStream.stream_id === selectedStream.id) {
                    stream = availableStream;
                    break;
                }
            }
        }

        if (!stream) {
            stream = data.streams[0];
        }

        change_room(stream, data.user);
        display_rooms(data.streams);
    }

    function display_rooms(streams) {
        let rooms = $('.room-buttons');
        rooms.empty();

        for (var i = 0; i < streams.length; ++i) {
            var availableStream = streams[i];
            let streamItem = $('<li class="nav-item"></li>')
            let streamButton = $('<a class="nav-link" href="#' + availableStream.stream_id + '" data-stream-id="' + availableStream.stream_id + '">' + availableStream.room_name + '</a>')
            if (availableStream.stream_id === selectedStream.id) {
                streamButton.addClass('active');
            }
            streamItem.append(streamButton);
            rooms.append(streamItem);
        }

        $('ul.room-buttons > li > a').on('click', switch_room);
    }

    function switch_room() {
        let roomButton = $(this);

        let activeRooms = $('.room-buttons .active');
        activeRooms = activeRooms.removeClass('active');
        roomButton.addClass('active');

        let streamId = roomButton.data('streamId');
        let stream = null;

        for (var i = 0; i < streamData.streams.length; ++i) {
            let availableStream = streamData.streams[i];
            if (availableStream.stream_id === streamId) {
                stream = availableStream;
                break;
            }
        }

        if (stream) {
            change_room(stream, streamData.user);
        }
    }

    function load_streams() {
        $.ajax({
            dataType: "json",
            method: "GET",
            url: "/streaming/feeds",
            success: update_streams
          });
    }

    load_streams();
    setInterval(load_streams, ROOM_REFRESH_INTERVAL_MS);
});
