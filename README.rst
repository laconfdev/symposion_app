symposion_app
=============

.. contents::

At this time, considerations have not been made to have the django project run
without additional infrastructure.

This can be configured in the future by switching the default
DEFAULT_FILE_STORAGE django default, and properly configuring django to load
subsequent configuration to switch this back to GCS on run in our testing
and production environments.

Login is a bit more complicated, the default flow redirects you out to a SAML
server, you can probably work something out with testshib.   But if you go to
/admin and log in, that will persist into /dashboard.


Required Configuration
----------------------

GCS
~~~

1. Set the bucket in settings.py
1. Store your Service Account JSON in a file
1. Set GOOGLE_APPLICATION_CREDENTIALS to your account file
1. Set GCS_BUCKET to your bucket name

SAML2
~~~~~

Please familiarise yourself with Shibboleth configuration and the general
flow of how to register a SP with an IDP.

If you send the SP metadata statically, validity time must be removed.

You will also need to register your IDP metadata here either statically or
remotely updated.

You may consider testing with testshib.

Configure signing and encryption keys, and configure them in the settings.py

Running a dev instance in Docker
--------------------------------

Assuming you have docker installed and working on your machine::
    ./make_dev_container.sh

will build you a container and run through the initial setup steps.
The last stage interactively creates a superuser account: you'll need
to interact with this for it to finish.

Once this has completed, you can hit http://localhost:28000/admin to
log into the admin interface. Once you're logged in,
http://localhost:28000 will take you to the dashboard.

Note that when you do this you're logged in as a superuser, so what
you see will be different from what a normal user will see.

Making migrations
~~~~~~~~~~~~~~~~~

If you make changes to the data model, you'll need to run "manage.py
makemigrations" to create a matching migration file. If you're on a
mac, or a system without python3, this can be difficult.

In such a case, you can use the above script to make and run a dev
container; then::
    docker exec -it symposion ./manage.py makemigrations


Running a dev instance in a VirtualEnv
--------------------------------------

Not all things are lovely, so we use constraints to force the versions we
we wish for without having to do anything ugly.  This may require a newer
version of pip than is packaged with distros virtualenv.

Note that this application is python 3 only so you must create your virtualenv
with a python3 interpreter.

- ``python3 -m venv venv``
- ``source ./venv/bin/activate``
- ``pip install -c constraints.txt -r requirements.txt``
- ``pip install -c constraints.txt -r vendored_requirements.txt``

Once your dev instance is up and running
----------------------------------------

Pre-post-start configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now that we are ready to start the app, we need to do initial setup, start
it, perform voodoo string magic, run more database jingling voodoo, and then
we can start it up and start looking around and customising the application.

``./manage.py migrate``
``./manage.py runserver``

Now we hit up the admin interface, find `symposion_proposals` `proposalkind`
and add `Talk` slug `talk`, `Tutorial`:`tutorial`, and `Miniconf`:`miniconf`.

Shut down and now run:
``./manage.py populate_inventory``

Now you can run the system and see how it goes.

Admin tasks
-----------

Admin Credentials
~~~~~~~~~~~~~~~~~

To create a local administrator account run:

``./manage.py createsuperuser``

and enter your username, email and password. This can then be used to login to: (http://localhost:8000/admin).

Base Data
~~~~~~~~~

On initial creation and whenever you reset the database you must reload the
basic data. This data is stored in the /fixtures directory. You can load this with:

``./manage.py loaddata ./fixtures/{conference,proposal_base,sites,sitetree}.json``

And to load the base admin users:

``./manage.py loaddata ./fixtures/admin_users.json``

admin1:Inq4JVQyQvWnqXDI

admin2:w0jYMJsM4S+1zHds

admin3:XzynbNH9Sw3pLPXe

Creating review permissions objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After conference Sections have been created, this command will add
Permission objects for those sections.

``./manage.py create_review_permissions``

CI/CD Workflow
~~~~~~~~~~~~~~

4 stages:

1. Dev Review apps
2. dev.lca2019.org
3. staging.lca2019.org
4. linux.conf.au/lca2019.org (not yet live)


- Driven by `.gitlab-ci.yml`

Stages 1 and 2 use sqlite databases; fixtures in the repo are loaded when the container starts but there is no persistent storage - *except* that stage 2 has some persistence of admin accounts. Stages 3 and 4 have (separate) mysql backends which persist between deploys.

Stages 2-4 use SSO for login; stage 1 has admin accounts defined in fixtures.

All commits to any dev/* branch create a dev review app; for instance, a commit on dev/tchaypo/reviewapps becomes available at http://dev-tchaypo-reviewapps.dev.lca2019.org (for now you'll need to add `35.189.5.101 dev-tchaypo-reviewapps.dev.lac2019.org` to /etc/hosts for this to work.)

  Once a commit is accepted into `/dev/2019` it will also be auto-deployed to `https://dev.lca2019.org`.

  Once it's been tested there and is ready for further testing it can be manually deployed to staging, which gives the first chance to test database migrations and to double-check the MySQL backend.

  The CI/CD processes can be monitored through Gitlab's CI/CD page https://gitlab.com/laconfdev/symposion_app/pipelines

  Manual deployments (eg, to staging and production) can be initiated from a pipeline's page - eg https://gitlab.com/laconfdev/symposion_app/pipelines/24003091 - after prior stages pass.

